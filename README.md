## 项目安装
```
npm install
```

### 项目运行
```
npm run serve
```

### 编译项目
```
npm run build
```

## 示例截图

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214245_00adb66f_4835367.png "ant1.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214254_4677557c_4835367.png "ant2.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214302_b36f13d8_4835367.png "ant3.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214310_a363ecb3_4835367.png "ant4.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214319_5e551a1b_4835367.png "ant5.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214328_b50b62e8_4835367.png "ant6.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214337_e99d7b78_4835367.png "ant7.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214346_4e5261c2_4835367.png "ant8.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214354_d3c1d47d_4835367.png "ant9.png")